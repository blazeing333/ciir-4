<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 16.06.2020
  Time: 9:28
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<html>
<head>
    <title>Users</title>
    <link href="/css/styles.css" rel="stylesheet" type="text/css">
</head>
<body>
<div align="center">
    <ul class="hr" >
        <li><a href="/home">Home</a></li>
        <li><a href="/users">Users</a></li>
        <li><a href="/products">Products</a></li>
        <li><a href="/places">Places</a></li>
        <li><a href="/distributors">Distributors</a></li>
    </ul>
</div>
<h3>Add user</h3>
<form action="/4/users" method="post">
    <input value="1" type="hidden" name="formNumber" id="formNumber1" type="text">
    <label>Name
        <input name="addName" type="text" class="name" id = "name">
    </label>
    <input type="submit" value="Add">
</form>

<h3>Delete user</h3>
<form action="/users" method="post">
    <input value="2" type="hidden" name="formNumber" id="formNumber2" type="text">
    <select multiple name="userId">
        <c:forEach items="${allUsers}" var="user">
            <option value="${user.id}">${user.id}</option>
        </c:forEach>
    </select>
    <input type="submit" value="Delete">
</form>

<h3>Update user</h3>
<form action="/users" method="post">
    <input value="3" type="hidden" name="formNumber" id="formNumber3" type="text">
    <select multiple name="userId">
        <c:forEach items="${allUsers}" var="user">
            <option value="${user.id}">${user.id}</option>
        </c:forEach>
    </select>
    <input name = "newName" class="input-field" type="text">
    <input type="submit" value="Update">
</form>

<h3>Add product to user</h3>
<form action="/users" method="post">
    <input value="5" type="hidden" name="formNumber" id="formNumber5" type="text">
    <select multiple name="userId">
        <c:forEach items="${allUsers}" var="user">
            <option value="${user.id}">${user.id}</option>
        </c:forEach>
    </select>
    <select multiple name="productId">
        <c:forEach items="${allProducts}" var="product">
            <option value="${product.id}">${product.id}</option>
        </c:forEach>
    </select>
    <input type="submit" value="Add">
</form>

<<h3>Filter</h3>
<form action="/users" method="post">
    <input value="4" type="hidden" name="formNumber" id="formNumber4" type="text">
    <select multiple name="userId">
        <option value="1">Over 600</option>
    </select>
    <input type="submit" value="Filter">
</form>

<h1>Users in data base</h1>
<table>
    <tr>
        <td>User id</td>
        <td>User name</td>
    </tr>
<c:forEach items="${allUsers}" var="user">
    <tr>
        <td>${user.id}</td>
        <td>${user.name}</td>
    </tr>

</c:forEach>
</table>
<tr>
    <td>User id</td>
    <td>User name</td>
    <td>Product id</td>
    <td>Product name</td>
</tr>
<table>
    <c:forEach items="${allUsers}" var="user" >
        <c:forEach items="${user.products}" var="product">
            <tr>
                <td>${user.id}</td>
                <td>${user.name}</td>
                <td>${product.id}</td>
                <td>${product.name}</td>
            </tr>
        </c:forEach>
    </c:forEach>

</table>
</body>
</html>
