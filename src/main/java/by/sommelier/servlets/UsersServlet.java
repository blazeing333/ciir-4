package by.sommelier.servlets;

import by.sommelier.control.ProductDAO;
import by.sommelier.control.UserDAO;
import by.sommelier.models.Product;
import by.sommelier.models.User;

import javax.persistence.criteria.CriteriaBuilder;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/users")
public class UsersServlet extends HttpServlet {
    private UserDAO userDAO;
    private ProductDAO productDAO;
    Integer flag = 0;

    @Override
    public void init() throws ServletException {
        this.userDAO = new UserDAO();
        this.productDAO = new ProductDAO();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if(flag == 0) {
            List<User> users = userDAO.readAll();
            List<Product> products = productDAO.readAll();
            req.setAttribute("allUsers", users);
            req.setAttribute("allProducts", products);
            req.getServletContext().getRequestDispatcher("/jsp/users.jsp").forward(req, resp);
        }
        else if( flag == 1){
            List<User> users = userDAO.userOver600();
            req.setAttribute("allUsers", users);
            req.getServletContext().getRequestDispatcher("/jsp/users.jsp").forward(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Integer choise = Integer.parseInt(req.getParameter("formNumber"));

        if(choise == 1){
        String name = req.getParameter("addName");
        userDAO.create(name);
        flag = 0;
        }
        else if(choise == 2){
            Integer id = Integer.parseInt(req.getParameter("userId"));
            userDAO.delete(id);
            flag = 0;
        }
        else if(choise == 3) {
            Integer id = Integer.parseInt(req.getParameter("userId"));
            String newName = req.getParameter("newName");

            userDAO.update(id, newName);
            flag = 0;
        }
        else if(choise == 4){
            flag = 1;
        }
        else if(choise == 5){
            Integer userId = Integer.parseInt(req.getParameter("userId"));
            Integer productId = Integer.parseInt(req.getParameter("productId"));
            userDAO.addProduct(userId, productId);
            flag = 0;
        }

        doGet(req, resp);
    }
}
