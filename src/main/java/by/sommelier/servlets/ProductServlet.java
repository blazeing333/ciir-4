package by.sommelier.servlets;

import by.sommelier.control.ProductDAO;
import by.sommelier.control.UserDAO;
import by.sommelier.models.Product;
import by.sommelier.models.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/products")
public class ProductServlet extends HttpServlet {

    private ProductDAO productDAO;

    @Override
    public void init() throws ServletException {
        this.productDAO = new ProductDAO();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        List<Product> products = productDAO.readAll();
        req.setAttribute("allProducts", products);
        req.getServletContext().getRequestDispatcher("/jsp/products.jsp").forward(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Integer choise = Integer.parseInt(req.getParameter("formNumber"));

        if(choise == 1){
            String name = req.getParameter("addName");
            productDAO.create(name);
        }
        else if(choise == 2){
            Integer id = Integer.parseInt(req.getParameter("productId"));
            productDAO.delete(id);
        }
        else if(choise == 3) {
            Integer id = Integer.parseInt(req.getParameter("productId"));
            String newName = req.getParameter("newName");

            productDAO.update(id, newName);
        }

        doGet(req, resp);
    }
}
