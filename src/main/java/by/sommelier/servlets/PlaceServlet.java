package by.sommelier.servlets;

import by.sommelier.control.PlaceDAO;
import by.sommelier.control.UserDAO;
import by.sommelier.models.Place;
import by.sommelier.models.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/places")
public class PlaceServlet extends HttpServlet {

    PlaceDAO placeDAO;

    @Override
    public void init() throws ServletException {
        this.placeDAO = new PlaceDAO();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

            List<Place> places = placeDAO.readAll();
            req.setAttribute("allPlaces", places);
            req.getServletContext().getRequestDispatcher("/jsp/places.jsp").forward(req, resp);

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Integer choise = Integer.parseInt(req.getParameter("formNumber"));

        if(choise == 1){
            String name = req.getParameter("addName");
            placeDAO.create(name);
        }
        else if(choise == 2){
            Integer id = Integer.parseInt(req.getParameter("placeId"));
            placeDAO.delete(id);
        }
        else if(choise == 3) {
            Integer id = Integer.parseInt(req.getParameter("placeId"));
            String newName = req.getParameter("newName");

            placeDAO.update(id, newName);
        }

        doGet(req, resp);
    }
}
