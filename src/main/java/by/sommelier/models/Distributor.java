package by.sommelier.models;

import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "ciir_distributor", uniqueConstraints = {
        @UniqueConstraint(columnNames = "id")})
public class Distributor {
    private Integer id;
    private String name;
    private List<Product> products = new ArrayList<>();

    public Distributor(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Distributor(Integer id) {
        this.id = id;
    }

    public Distributor(){}

    public Distributor(String name) {
        this.name = name;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, insertable = true, updatable = false)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "name", nullable = true, insertable = true, updatable = true)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "ciir_distribution",
            joinColumns = @JoinColumn(name = "distributor_id" ),
            inverseJoinColumns = @JoinColumn(name = "product_id"))
    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Distributor distributor = (Distributor) o;
        return id.equals(distributor.id) &&
                name.equals(distributor.name);
    }

    @Override
    public String toString() {
        return "id:"+ this.id + "; name:"+ this.name;
    }
}
