package by.sommelier.control;

import by.sommelier.models.Place;
import by.sommelier.models.Product;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.List;
import java.util.Scanner;

public class PlaceDAO implements CrudDAO<Place> {
    private Session session;
    SessionFactory sessionFactory;
    Configuration configuration;

    public PlaceDAO() {
        this.configuration = new Configuration();
        configuration.configure("Hibernate.cfg.xml");

        this.sessionFactory = configuration.buildSessionFactory();

    }

    public void closeSessionFactory(){
        sessionFactory.close();
    }

    public void closeSession(){
        session.close();
    }

    public void openSession(){
        this.session = sessionFactory.openSession();
    }

    @Override
    public void create(String name) {
        openSession();

        session.beginTransaction();
        session.save(new Place(name));
        session.getTransaction().commit();
        closeSession();
    }

    @Override
    public List<Place> readAll() {
        openSession();
        List<Place> places = (List<Place>) session.createQuery("from Place").list();
        closeSession();
        return places;
    }

    @Override
    public void update(Integer id, String name) {
        openSession();
        Place place = session.get(Place.class, id);
        session.beginTransaction();
        place.setName(name);
        session.update(place);
        session.getTransaction().commit();
        closeSession();
    }

    @Override
    public void delete(Integer id) {
        openSession();

        session.beginTransaction();
        Place place = (Place) session.get(Place.class, id);
        session.delete(place);
        session.getTransaction().commit();
        closeSession();
    }

    public void addProduct(Integer placeId, Integer productId){
        openSession();
        session.beginTransaction();

        Product product = session.get(Product.class, productId);
        Place place = session.get(Place.class, placeId);

        place.getProducts().add(product);
        session.update(place);
        session.getTransaction().commit();
        closeSession();
    }

    public void deleteProductFromPlace(Integer placeId, Integer productId){
        openSession();
        session.beginTransaction();

        Product product = session.get(Product.class, productId);
        Place place = session.get(Place.class, placeId);

        place.getProducts().remove(product);
        session.update(place);
        session.getTransaction().commit();
        closeSession();

    }

    public List<Product> showAllPlaceProducts(Integer id){
        openSession();

        Place place = session.get(Place.class, id);
        closeSession();
        return place.getProducts();
    }

    @Override
    protected void finalize() throws Throwable {
        sessionFactory.close();
    }
}