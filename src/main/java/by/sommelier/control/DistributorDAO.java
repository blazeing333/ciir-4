package by.sommelier.control;

import by.sommelier.models.Distributor;
import by.sommelier.models.Product;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.util.List;
import java.util.Scanner;

public class DistributorDAO implements CrudDAO<Distributor> {
    private Session session;
    SessionFactory sessionFactory;

    public DistributorDAO() {
        Configuration configuration = new Configuration();
        configuration.configure("Hibernate.cfg.xml");

        this.sessionFactory = configuration.buildSessionFactory();

    }

    public void openSession(){
        this.session = sessionFactory.openSession();
    }

    public void closeSession(){

        session.close();
    }

    public void closeSessionFactory(){
        sessionFactory.close();
    }

    @Override
    public void create(String name) {
        openSession();

        session.beginTransaction();
        session.save(new Distributor(name));
        session.getTransaction().commit();
        closeSession();
    }

    @Override
    public List<Distributor> readAll() {
        openSession();
        List<Distributor> distributors = (List<Distributor>) session.createQuery("from Distributor ").list();

        closeSession();
        return distributors;
    }

    @Override
    public void update(Integer id, String name) {
        openSession();

        session.beginTransaction();
        Distributor distributor = new Distributor(id, name);
        session.update(distributor);
        session.getTransaction().commit();
        closeSession();
    }

    @Override
    public void delete(Integer id) {
        openSession();

        session.beginTransaction();
        Distributor distributor = (Distributor) session.get(Distributor.class, id);
        session.delete(distributor);
        session.getTransaction().commit();
        closeSession();
    }

    public void addProduct(Integer distributorId, Integer productId){
        openSession();
        session.beginTransaction();

        Product product = session.get(Product.class, productId);
        Distributor distributor = session.get(Distributor.class, distributorId);

        distributor.getProducts().add(product);
        session.update(distributor);
        session.getTransaction().commit();
        closeSession();
    }

    public void deleteProductFromDistributor(Integer distributorId, Integer productId){
        openSession();
        session.beginTransaction();

        Product product = session.get(Product.class, productId);
        Distributor distributor = session.get(Distributor.class, distributorId);

        distributor.getProducts().remove(product);
        session.update(distributor);
        session.getTransaction().commit();

        closeSession();

    }

    public List<Product> showAllDistributorProducts(Integer id){
        openSession();

        Distributor distributor = session.get(Distributor.class, id);

        closeSession();
        return distributor.getProducts();
    }

    @Override
    protected void finalize() throws Throwable {
        sessionFactory.close();
    }
}