package by.sommelier.control;

import by.sommelier.models.Product;
import by.sommelier.models.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import javax.swing.event.InternalFrameEvent;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

public class UserDAO implements CrudDAO<User> {
    private Session session;
    SessionFactory sessionFactory;
    Configuration configuration;

    public UserDAO() {
        this.configuration = new Configuration();
        this.configuration = configuration.configure("Hibernate.cfg.xml");
        this.sessionFactory = configuration.buildSessionFactory();
    }

    public void closeSessionFactory(){
        sessionFactory.close();
    }

    public void closeSession(){

        session.close();
    }

    public void openSession(){
        this.session = sessionFactory.openSession();
    }

    @Override
    public void create(String name) {
        openSession();

        session.beginTransaction();
        session.save(new User(name));
        session.getTransaction().commit();
        closeSession();
    }

    @Override
    public List<User> readAll() {
        openSession();
        List<User> users = (List<User>) session.createQuery("from User").list();
        closeSession();
        return users;
    }

    @Override
    public void update(Integer id, String name) {
        openSession();
        User user = session.get(User.class, id);
        session.beginTransaction();
        user.setName(name);
        session.update(user);
        session.getTransaction().commit();
        closeSession();

    }

    @Override
    public void delete(Integer id) {
        openSession();

        session.beginTransaction();
        User user = (User) session.get(User.class, id);
        session.delete(user);
        session.getTransaction().commit();
        closeSession();
    }

    public void addProduct(Integer userId, Integer productId ){
        openSession();

        session.beginTransaction();

        Product product = session.get(Product.class, productId);
        User user = session.get(User.class, userId);

        user.getProducts().add(product);
        session.update(user);
        session.getTransaction().commit();
        closeSession();
    }

    public void deleteProductFromUser(Integer userId, Integer productId){
        openSession();
        session.beginTransaction();

        Product product = session.get(Product.class, productId);
        User user = session.get(User.class, userId);

        user.getProducts().remove(product);
        session.update(user);
        session.getTransaction().commit();
        closeSession();
    }

    public List<Product> showAllUserProducts(Integer userId){
        openSession();

        User user = session.get(User.class, userId);
        closeSession();
        return user.getProducts();
    }

    public List<User> userOver600(){
        openSession();
        List<User> users = (List<User>) session.createQuery("from User user where user.id > 600").list();
        closeSession();
        return users;
    }

    @Override
    protected void finalize() throws Throwable {
        sessionFactory.close();
    }
}
