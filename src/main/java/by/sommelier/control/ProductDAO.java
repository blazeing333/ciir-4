package by.sommelier.control;

import by.sommelier.models.Product;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.util.List;
import java.util.Scanner;

public class ProductDAO implements CrudDAO<Product> {
    private Session session;
    SessionFactory sessionFactory;
    Configuration configuration;

    public ProductDAO() {
        this.configuration = new Configuration();
        configuration.configure("Hibernate.cfg.xml");
        this.sessionFactory = configuration.buildSessionFactory();

    }

    public void closeSessionFactory(){
        sessionFactory.close();
    }

    public void closeSession(){

        session.close();
    }

    public void openSession(){
        this.session = sessionFactory.openSession();
    }

    @Override
    public void create(String name) {
        openSession();
        session.beginTransaction();
        session.save(new Product(name));
        session.getTransaction().commit();
        closeSession();
    }

    @Override
    public List<Product> readAll() {
        openSession();
        List<Product> products = (List<Product>) session.createQuery("from Product").list();

        closeSession();
        return products;
    }

    @Override
    public void update(Integer id, String name) {
        openSession();

        session.beginTransaction();
        Product product = new Product(id, name);
        session.update(product);
        session.getTransaction().commit();
        closeSession();
    }

    @Override
    public void delete(Integer id) {
        openSession();

        session.beginTransaction();
        Product product = (Product) session.get(Product.class, id);
        session.delete(product);
        session.getTransaction().commit();
        closeSession();
    }

    @Override
    protected void finalize() throws Throwable {
        sessionFactory.close();
    }
}