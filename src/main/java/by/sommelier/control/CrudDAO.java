package by.sommelier.control;

import java.util.List;

public interface CrudDAO<T> {
    public void create(String name);

    public List<T> readAll();

    public void update(Integer id, String name);

    public void delete(Integer id);

}