<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 16.06.2020
  Time: 10:44
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<html>
<head>
    <title>Users</title>
    <link href="/css/styles.css" rel="stylesheet" type="text/css">
</head>
<body>
<div align="center">
    <ul class="hr" >
        <li><a href="/4/home">Home</a></li>
        <li><a href="/4/users">Users</a></li>
        <li><a href="/4/products">Products</a></li>
        <li><a href="/4/places">Places</a></li>
        <li><a href="/4/distributors">Distributors</a></li>
    </ul>
</div>
<h3>Add place</h3>
<form action="/4/places" method="post">
    <input value="1" type="hidden" name="formNumber" id="formNumber1" type="text">
    <label>Name
        <input name="addName" type="text" class="name" id = "name">
    </label>
    <input type="submit" value="Add">
</form>

<h3>Delete place</h3>
<form action="/4/places" method="post">
    <input value="2" type="hidden" name="formNumber" id="formNumber2" type="text">
    <select multiple name="placeId">
        <c:forEach items="${allPlaces}" var="place">
            <option value="${place.id}">${place.id}</option>
        </c:forEach>
    </select>
    <input type="submit" value="Delete">
</form>

<h3>Update place</h3>
<form action="/4/places" method="post">
    <input value="3" type="hidden" name="formNumber" id="formNumber3" type="text">
    <select multiple name="placeId">
        <c:forEach items="${allPlaces}" var="place">
            <option value="${place.id}">${place.id}</option>
        </c:forEach>
    </select>
    <input name = "newName" class="input-field" type="text">
    <input type="submit" value="Update">
</form>

<h1>Places in data base</h1>
<table>
    <tr>
        <td>Place id</td>
        <td>Place name</td>
    </tr>
    <c:forEach items="${allPlaces}" var="place">
        <tr>
            <td>${place.id}</td>
            <td>${place.name}</td>
        </tr>

    </c:forEach>
</table>
<tr>
    <td>Place id</td>
    <td>Place name</td>
    <td>Product id</td>
    <td>Product name</td>
</tr>
<table>
    <c:forEach items="${allPlaces}" var="place" >
        <c:forEach items="${place.products}" var="product">
            <tr>
                <td>${place.id}</td>
                <td>${place.name}</td>
                <td>${product.id}</td>
                <td>${product.name}</td>
            </tr>
        </c:forEach>
    </c:forEach>

</table>
</body>
</html>

